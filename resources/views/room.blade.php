@extends('layouts.app')

@section('content')
    @push('js')
        <script>

            socket.on('joined', function (data) {
                data = JSON.parse(data);

                document.getElementById('message_room').innerHTML += `<li class="left clearfix">
                                     <span class="chat-img1 pull-left">
                                        <i class="fa fa-user fa-4x"></i>
                                     </span>
                                    <div class="chat-body1 clearfix">
                                        <p>${data.user.name} has joined</p>
                                    </div>
                                </li>`;

                $.each(data.user_list, function (index, user) {

                    user = user.user;

                    if (!$('.users').hasClass(`user_${user.id}`)) {

                        document.getElementById('user_list').innerHTML += `<li class="left users clearfix user_${user.id}" id="user_${user.id}">
                                     <span class="chat-img pull-left">
                                         <i class="fa fa-user fa-3x"></i>
                                     </span>

                                    <div class="chat-body clearfix">
                                        <div class="header_sec">
                                            <strong class="primary-font">${user.name}</strong> <strong
                                                class="pull-right">
                                                09:45AM</strong>
                                        </div>
                                    </div>
                                </li>`;

                    }
                })
            })

            socket.on('user_leave_room', function (data) {

                data = JSON.parse(data);

                document.getElementById('message_room').innerHTML += `<li class="left clearfix">
                                     <span class="chat-img1 pull-left">
                                        <i class="fa fa-user fa-4x"></i>
                                     </span>
                                    <div class="chat-body1 clearfix">
                                        <p>${data.user.name} has left</p>
                                    </div>
                                </li>`;

                $(`#user_${data.user.id}`).remove();
            })

            socket.emit('join_room', {room: "{{$roomname}}"});

            function send_to_room() {
                var message = document.getElementById('message').value;
                var room = document.getElementById('room').value;
                socket.emit('send_to_room', {room, message});
            }

            window.onbeforeunload = function () {
                socket.emit('leave_room', {room: "{{$roomname}}"});
            };

            var send_text = document.querySelector('.send_text');

            function send_msg() {
                var message = $('.room_text').val();
                socket.emit('send_room_msg', {message, room: "{{$roomname}}"});
                $('.room_text').val('');
            }

            socket.on('new_msg', function (data) {
                data = JSON.parse(data);

                document.getElementById('message_room').innerHTML += `<li class="left clearfix">
                                     <span class="chat-img1 pull-left">
                                        <i class="fa fa-user fa-4x"></i>
                                     </span>
                                    <div class="chat-body1 clearfix">
                                        <p>${data.user.name} : ${data.message}</p>
                                    </div>
                                </li>`;

            })

        </script>
    @endpush

    <div class="main_section">
        <div class="container">
            <div class="chat_container">
                <div class="col-sm-3 chat_sidebar">
                    <div class="row">
                        <div id="custom-search-input" style="width: 100%;">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Conversation"/>
                                <button class="btn btn-danger" type="button">
                                    <span class=" glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <div class="member_list" style="width: 100%;">
                            <ul class="list-unstyled" id="user_list">


                            </ul>
                        </div>
                    </div>
                </div>
                <!--chat_sidebar-->


                <div class="col-sm-9 message_section">
                    <div class="row">
                        <div class="new_message_head">
                            <div class="pull-left">
                                <button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenu1"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-cogs" aria-hidden="true"></i> Setting
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Profile</a></li>
                                        <li><a href="#">Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--new_message_head-->

                        <div class="chat_area">
                            <ul class="list-unstyled" id="message_room">


                            </ul>
                        </div><!--chat_area-->
                        <div class="message_write">
                            <textarea class="form-control room_text" placeholder="type a message"></textarea>
                            <div class="clearfix"></div>
                            <div class="chat_bottom"><a href="#" class="pull-left upload_btn"><i
                                        class="fa fa-cloud-upload" aria-hidden="true"></i>
                                    Add Files</a>
                                <button onclick="send_msg()" class="pull-right btn btn-success send_text">
                                    Send
                                </button>
                            </div>
                        </div>
                    </div>
                </div> <!--message_section-->
            </div>
        </div>
    </div>
@endsection
