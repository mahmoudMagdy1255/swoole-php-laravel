@extends('layouts.app')

@section('content')
    @push('js')
        <script>
            function sendMsg() {
                var message = document.getElementById('message').value;
                var to = document.getElementById('to').value;
                socket.emit('sendmsg', {to, message});

            }

            function broadcast() {
                var message = document.getElementById('broadcast').value;
                socket.emit('broadcast', {message});
            }

        </script>
    @endpush

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <textarea name="broadcast" id="broadcast"></textarea>
                        <button id="send" onclick="broadcast()">Send Broadcast</button>

                        <hr>

                        <input type="text" name="to" id="to">

                        <textarea name="message" id="message"></textarea>

                        <button id="send" onclick="sendMsg()">Send Message</button>

                        <span id="my_message"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
