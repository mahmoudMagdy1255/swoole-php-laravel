<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use SwooleTW\Http\Websocket\Facades\Room;
use SwooleTW\Http\Websocket\Facades\Websocket;

class WebsocketController extends Controller
{

    public function room($type, $data)
    {
        $type == 'join' ? \App\Room::create($data) : \App\Room::where($data)->delete();
    }

    public function user()
    {
        return User::find(Websocket::getUserId());
    }

    public static function json($data)
    {
        return addslashes(json_encode($data));
    }

    public function sendmsg($websocket, $data)
    {
        Websocket::toUserId($data['to'])->emit('message', $data['message']);
        Websocket::toUserId($data['to'])->emit('testjson', addslashes(json_encode(['user1' => 'php', 'user2' => 'swoole'])));
    }

    public function connect($websocket, Request $request)
    {
        $user_id = $request->request->all()['user_id'];
        Websocket::loginUsingId($user_id);
        echo "We Have A New USer" . Websocket::getUserId() . "\n";
    }

    public function disconnect($websocket)
    {
        echo "User Id " . $websocket->getUserId() . "is Left \n";
    }

    public function broadcast($websocket, $data)
    {
        $websocket->broadcast()->emit('message', $data['message']);
    }

    public function join_room($websocket, $data)
    {
        Websocket::join($data['room']);
        Room::add(Websocket::getUserId(), $data['room']);
        self::room('join', ['user_id' => Websocket::getUserId(), 'name' => $data['room']]);
        $user_list = \App\Room::where('name', $data['room'])->with('user')->get();
        Websocket::to($data['room'])->emit('joined', self::json(['user' => $this->user(), 'user_list' => $user_list]));
    }

    public function leave_room($websocket, $data)
    {
        Websocket::leave($data['room']);
        Room::delete(Websocket::getUserId(), $data['room']);
        Websocket::to($data['room'])->emit('user_leave_room', self::json(['user' => $this->user()]));
        self::room('leave', ['user_id' => Websocket::getUserId(), 'name' => $data['room']]);
    }

    public function send_room_msg($websocket, $data)
    {
        Websocket::to($data['room'])->emit('new_msg', self::json(['user' => $this->user(), 'message' => $data['message']]));
    }
}
