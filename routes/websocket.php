<?php


use SwooleTW\Http\Websocket\Facades\Websocket;

/*
|--------------------------------------------------------------------------
| Websocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register websocket events for your application.
|
*/
Websocket::on('connect', '\App\Http\Controllers\WebsocketController@connect');
Websocket::on('disconnect', '\App\Http\Controllers\WebsocketController@disconnect');
Websocket::on('sendmsg', '\App\Http\Controllers\WebsocketController@sendmsg');
Websocket::on('broadcast', '\App\Http\Controllers\WebsocketController@broadcast');
Websocket::on('join_room', '\App\Http\Controllers\WebsocketController@join_room');
Websocket::on('leave_room', '\App\Http\Controllers\WebsocketController@leave_room');
Websocket::on('send_room_msg', '\App\Http\Controllers\WebsocketController@send_room_msg');
